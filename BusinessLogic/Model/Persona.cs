﻿using BusinessLogic.Model.Enum;
using System;

namespace BusinessLogic.Model
{
	public abstract class Persona
	{
		public int IdPersona { get; set; }
		public string Nombre { get; set; }
		public string ApellidoPaterno { get; set; }
		public string ApellidoMaterno { get; set; }
		public string Email { get; set; }
		public DateTime FechaNacimiento { get; set; }
		public Género Género { get; set; }
	}
}
