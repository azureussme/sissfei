﻿using System.Collections.Generic;

namespace BusinessLogic.Model
{
	public class Organización
	{
		public int IdOrganización { get; set; }
		public string Dirección { get; set; }
		public string Nombre { get; set; }
		public string Teléfono { get; set; }
		// public List<Encargado> Encargados { get; set; }
		public List<Proyecto> Proyectos { get; set; }
	}
}
