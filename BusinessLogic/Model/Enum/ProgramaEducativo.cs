﻿namespace BusinessLogic.Model.Enum
{
	public enum ProgramaEducativo
	{
		IngenieríaDeSoftware,
		RedesYServicios,
		TecnologíasComputacionales,
		Estadística
	}
}
