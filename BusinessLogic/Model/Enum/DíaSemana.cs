﻿namespace BusinessLogic.Model.Enum
{
	public enum DíaSemana
	{
		Domingo,
		Lunes,
		Martes,
		Miércoles,
		Jueves,
		Viernes,
		Sábado
	}
}
