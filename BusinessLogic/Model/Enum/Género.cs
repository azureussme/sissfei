﻿namespace BusinessLogic.Model.Enum
{
	public enum Género
	{
		Masculino,
		Femenino,
		Indefinido
	}
}
