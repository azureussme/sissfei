﻿using BusinessLogic.Model.Enum;

namespace BusinessLogic.Model
{
	public class Horario
	{
		public int IdHorario { get; set; }
		public DíaSemana DíaSemana { get; set; }
		public string HoraInicio { get; set; }
		public string HoraFin { get; set; }
		public string Lugar { get; set; }
	}
}
