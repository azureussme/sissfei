﻿using BusinessLogic.Model.Enum;
using System.Collections.Generic;

namespace BusinessLogic.Model
{
	public class TécnicoAcadémico : Persona
	{
		public int IdTécnicoAcadémico { get; set; }
		public string NúmeroPersonal { get; set; }
		public ProgramaEducativo ProgramaEducativo { get; set; }
		public List<Horario> Horarios { get; set; }
	}
}
