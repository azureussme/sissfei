﻿using System.Collections.Generic;

namespace BusinessLogic.Model
{
	public class Proyecto
	{
		public int IdProyecto { get; set; }
		public string Actividad { get; set; }
		public string Description { get; set; }
		public string Duración { get; set; }
		public string FunciónActividad { get; set; }
		public string Metodología { get; set; }
		public string Nombre { get; set; }
		public int NúmeroParticipantes { get; set; }
		public string Requisitos { get; set; }
		public string HorarioProbable { get; set; }
		public List<Horario> Horarios { get; set; }
	}
}
