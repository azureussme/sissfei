﻿using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
	public interface IOrganizaciónDAO
	{
		int AgregarOrganización(Organización organización);
		void EditarOrganización(Organización organización);
		void EliminarOrganización(int IdOrganización);
	}
}
