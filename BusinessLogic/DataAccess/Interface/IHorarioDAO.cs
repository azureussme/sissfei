﻿using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
	public interface IHorarioDAO
	{
		int AgregarHorarioATécnicoAcadémico(Horario horario, int IdTécnicoAcadémico);
		int AgregarHorarioAProyecto(Horario horario, int IdProyecto);
		void EliminarHorarioDeTécnicoAcadémico(int IdHorario);
		void EliminarHorarioDeProyecto(int IdHorario);
	}
}
