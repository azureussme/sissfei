﻿using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
	interface IProyectoDAO
	{
		int AgregarProyecto(Proyecto proyecto, int IdOrganización);
		void ModificarProyecto(Proyecto proyecto);
		void EliminarProyecto(int IdProyecto);
	}
}
