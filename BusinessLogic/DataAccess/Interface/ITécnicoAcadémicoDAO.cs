﻿using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
	public interface ITécnicoAcadémicoDAO
	{
		int AgregarTécnicoAcadémico(TécnicoAcadémico técnicoAcadémico);
		TécnicoAcadémico GetTécnicoAcadémico(int IdTécnicoAcadémico);
		bool EliminarTécnicoAcadémico(int IdTécnicoAcadémico);
	}
}
