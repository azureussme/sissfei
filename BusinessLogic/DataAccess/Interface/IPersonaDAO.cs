﻿using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interface
{
	interface IPersonaDAO
	{
		long AgregarPersona(Persona persona);
		void EditarPersona(Persona persona);
		bool EliminarPersona(int IdPersona);
	}
}
