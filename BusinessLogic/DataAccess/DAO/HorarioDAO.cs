﻿using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Model;
using Database;
using System;

namespace BusinessLogic.DataAccess.DAO
{
	public class HorarioDAO : IHorarioDAO
	{
		private int AgregarHorario(Horario horario)
		{
			var insertHorarioCommand = new Command(
				"INSERT INTO SISSFEI.Horario (" +
					"DíaSemana, " +
					"HoraInicio, " +
					"HoraFin, " +
					"Lugar" +
				") VALUES (" +
					"@DíaSemana, " +
					"@HoraInicio, " +
					"@HoraFin, " +
					"@Lugar" +
				");"
			);

			insertHorarioCommand.AddParameter("@DíaSemana", horario.DíaSemana.ToString());
			insertHorarioCommand.AddParameter("@HoraInicio", horario.HoraInicio);
			insertHorarioCommand.AddParameter("@HoraFin", horario.HoraFin);
			insertHorarioCommand.AddParameter("@Lugar", horario.Lugar);

			var result = insertHorarioCommand.Execute();
			int IdHorario = 0;

			if (result > 0)
			{
				IdHorario = insertHorarioCommand.GetLastInsertId();
				horario.IdHorario = IdHorario;
			}
			else
			{
				throw new Exception("No se pudo agregar el Horario.");
			}
			return IdHorario;
		}

		public int AgregarHorarioAProyecto(Horario horario, int IdProyecto)
		{
			var IdHorario = AgregarHorario(horario);

			var linkHorarioToProyectoCommand = new Command(
				"INSERT INTO SISSFEI.ProyectoTieneHorario (" +
					"IdProyecto, " +
					"IdHorario" +
				") VALUES (" +
					"@IdProyecto, " +
					"@IdHorario" +
				");"
			);

			linkHorarioToProyectoCommand.AddParameter("@IdProyecto", IdProyecto);
			linkHorarioToProyectoCommand.AddParameter("@IdHorario", IdHorario);

			var result = linkHorarioToProyectoCommand.Execute();

			if (result == 0)
			{
				throw new Exception("No se pudo agregar horario al proyecto con ID: " + IdProyecto);
			}

			return IdHorario;
		}

		public int AgregarHorarioATécnicoAcadémico(Horario horario, int IdTécnicoAcadémico)
		{
			var IdHorario = AgregarHorario(horario);

			var linkHorarioToTécnicoAcadémicoCommand = new Command(
				"INSERT INTO SISSFEI.TécnicoAcadémicoTieneHorario (" +
					"IdTécnicoAcadémico, " +
					"IdHorario" +
				") VALUES (" +
					"@IdTécnicoAcadémico, " +
					"@IdHorario" +
				");"
			);

			linkHorarioToTécnicoAcadémicoCommand.AddParameter("@IdTécnicoAcadémico", IdTécnicoAcadémico);
			linkHorarioToTécnicoAcadémicoCommand.AddParameter("@IdHorario", IdHorario);

			var result = linkHorarioToTécnicoAcadémicoCommand.Execute();

			if (result == 0)
			{
				throw new Exception("No se pudo agregar horario al técnico académico con ID: " + IdTécnicoAcadémico);
			}

			return IdHorario;
		}

		public void EliminarHorarioDeTécnicoAcadémico(int IdHorario)
		{
			var deleteLinkCommand = new Command(
				"DELETE FROM SISSFEI.TécnicoAcadémicoTieneHorario " +
				"WHERE " +
					"IdHorario = @IdHorario;"
			);
			deleteLinkCommand.AddParameter("@IdHorario", IdHorario);

			var result = deleteLinkCommand.Execute();

			if (result > 0)
			{
				EliminarHorario(IdHorario);
			}
			else
			{
				throw new Exception("No se pudo eliminar horario de la tabla vinculante TécnicoAcadémico con el ID: " + IdHorario);
			}
		}

		public void EliminarHorarioDeProyecto(int IdHorario)
		{
			var deleteLinkCommand = new Command(
				"DELETE FROM SISSFEI.ProyectoTieneHorario " +
				"WHERE " +
					"IdHorario = @IdHorario;"
			);
			deleteLinkCommand.AddParameter("@IdHorario", IdHorario);

			var result = deleteLinkCommand.Execute();

			if (result > 0)
			{
				EliminarHorario(IdHorario);
			}
			else
			{
				throw new Exception("No se pudo eliminar horario de la tabla vinculante Proyecto con el ID: " + IdHorario);
			}
		}

		private void EliminarHorario(int IdHorario)
		{
			var deleteHorarioCommand = new Command(
				"DELETE FROM SISSFEI.Horario " +
				"WHERE " +
					"IdHorario = @IdHorario;"
			);
			deleteHorarioCommand.AddParameter("@IdHorario", IdHorario);

			var result = deleteHorarioCommand.Execute();

			if (result == 0)
			{
				throw new Exception("No se pudo eliminar horario con el ID: " + IdHorario);
			}
		}
	}
}
