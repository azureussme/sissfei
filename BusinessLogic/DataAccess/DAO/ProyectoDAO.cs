﻿using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Model;
using Database;
using System;

namespace BusinessLogic.DataAccess.DAO
{
	public class ProyectoDAO : IProyectoDAO
	{
		private HorarioDAO horarioDAO = new HorarioDAO();

		public int AgregarProyecto(Proyecto proyecto, int IdOrganización)
		{
			var command = new Command(
				"INSERT INTO SISSFEI.Proyecto " +
					"(Actividad, Descripción, Duración, FunciónActividad, Metodología, Nombre, NúmeroParticipantes, Requisitos, IdOrganización) " +
				"VALUES " +
					"(@Actividad, @Descripción, @Duración, @FunciónActividad, @Metodología, @Nombre, @NúmeroParticipantes, @Requisitos, @IdOrganización);"
			);

			command.AddParameter("@Actividad", proyecto.Actividad);
			command.AddParameter("@Descripción", proyecto.Description);
			command.AddParameter("@Duración", proyecto.Duración);
			command.AddParameter("@FunciónActividad", proyecto.FunciónActividad);
			command.AddParameter("@Metodología", proyecto.Metodología);
			command.AddParameter("@Nombre", proyecto.Nombre);
			command.AddParameter("@NúmeroParticipantes", proyecto.NúmeroParticipantes);
			command.AddParameter("@Requisitos", proyecto.Requisitos);
			command.AddParameter("@IdOrganización", IdOrganización);

			var result = command.Execute();
			if (result == 0)
			{
				throw new Exception("No se pudo agregar el proyecto.");
			}

			var IdProyecto = command.GetLastInsertId();

			if (proyecto.Horarios != null)
			{
				foreach (Horario horario in proyecto.Horarios)
				{
					var IdHorario = horarioDAO.AgregarHorarioAProyecto(horario, IdProyecto);
					horario.IdHorario = IdHorario;
				}
			}

			return IdProyecto;
		}

		public void EliminarProyecto(int IdProyecto)
		{
			throw new System.NotImplementedException();
		}

		public void ModificarProyecto(Proyecto proyecto)
		{
			throw new System.NotImplementedException();
		}
	}
}
