﻿using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Model;
using Database;
using System;

namespace BusinessLogic.DataAccess.DAO
{
	public class PersonaDAO : IPersonaDAO
	{
		public long AgregarPersona(Persona persona)
		{
			var command = new Command(
				"INSERT INTO SISSFEI.Persona " +
					"(Nombre, ApellidoPaterno, ApellidoMaterno, Email, FechaNacimiento, Género) " +
				"VALUES " +
					"(@Nombre, @ApellidoPaterno, @ApellidoMaterno, @Email, @FechaNacimiento, @Género);"
			);

			command.AddParameter("@Nombre", persona.Nombre);
			command.AddParameter("@ApellidoPaterno", persona.ApellidoPaterno);
			command.AddParameter("@ApellidoMaterno", persona.ApellidoMaterno);
			command.AddParameter("@Email", persona.Email);
			command.AddParameter("@FechaNacimiento", persona.FechaNacimiento.ToString("MM/dd/yyyy"));
			command.AddParameter("@Género", persona.Género.ToString());

			var result = command.Execute();

			if (result == 0)
			{
				throw new Exception("No se pudo agregar la persona.");
			}

			var IdPersona = command.GetLastInsertId();
			persona.IdPersona = IdPersona;

			return IdPersona;
		}

		public void EditarPersona(Persona persona)
		{
			if (persona == null || persona.IdPersona <= 0)
			{
				throw new Exception("Error al editar persona, no cuenta con ID");
			}

			var command = new Command(
				"UPDATE SISSFEI.Persona " +
				"SET " +
					"Nombre = @Nombre, " +
					"ApellidoPaterno = @ApellidoPaterno, " +
					"ApellidoMaterno = @ApellidoMaterno, " +
					"Email = @Email, " +
					"FechaNacimiento = @FechaNacimiento, " +
					"Género = @Género " +
				"WHERE " +
					"IdPersona = @IdPersona;"
			);

			command.AddParameter("@IdPersona", persona.IdPersona);
			command.AddParameter("@Nombre", persona.Nombre);
			command.AddParameter("@ApellidoPaterno", persona.ApellidoPaterno);
			command.AddParameter("@ApellidoMaterno", persona.ApellidoMaterno);
			command.AddParameter("@Email", persona.Email);
			command.AddParameter("@FechaNacimiento", persona.FechaNacimiento.ToString("MM/dd/yyyy"));
			command.AddParameter("@Género", persona.Género.ToString());

			var result = command.Execute();

			if (result == 0)
			{
				throw new Exception("Error al modificar la persona con ID: " + persona.IdPersona);
			}
		}

		public bool EliminarPersona(int IdPersona)
		{
			throw new System.NotImplementedException();
		}
	}
}
