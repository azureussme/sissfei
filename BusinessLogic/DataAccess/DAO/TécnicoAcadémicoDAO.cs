﻿using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Model;
using Database;
using System;

namespace BusinessLogic.DataAccess.DAO
{
	public class TécnicoAcadémicoDAO : ITécnicoAcadémicoDAO
	{
		private PersonaDAO personaDAO = new PersonaDAO();
		private HorarioDAO horarioDAO = new HorarioDAO();

		public int AgregarTécnicoAcadémico(TécnicoAcadémico técnicoAcadémico)
		{
			var IdPersona = personaDAO.AgregarPersona(técnicoAcadémico);

			var command = new Command(
				"INSERT INTO SISSFEI.TécnicoAcadémico (" +
					"IdPersona, " +
					"NúmeroPersonal, " +
					"ProgramaEducativo" +
				") VALUES (" +
					"@IdPersona, " +
					"@NúmeroPersonal, " +
					"@ProgramaEducativo" +
				");"
			);

			command.AddParameter("@IdPersona", IdPersona);
			command.AddParameter("@NúmeroPersonal", técnicoAcadémico.NúmeroPersonal);
			command.AddParameter("@ProgramaEducativo", técnicoAcadémico.ProgramaEducativo.ToString());

			var result = command.Execute();

			if (result == 0)
			{
				throw new Exception("No se pudo agregar el Técnico Académico");
			}

			var IdTécnicoAcadémico = command.GetLastInsertId();
			técnicoAcadémico.IdTécnicoAcadémico = IdTécnicoAcadémico;

			if (técnicoAcadémico.Horarios != null)
			{
				foreach (Horario horario in técnicoAcadémico.Horarios)
				{
					var IdHorario = horarioDAO.AgregarHorarioATécnicoAcadémico(horario, IdTécnicoAcadémico);
					horario.IdHorario = IdHorario;
				}
			}

			return IdTécnicoAcadémico;
		}

		public bool EliminarTécnicoAcadémico(int IdTécnicoAcadémico)
		{
			throw new System.NotImplementedException();
		}

		public TécnicoAcadémico GetTécnicoAcadémico(int IdTécnicoAcadémico)
		{
			throw new System.NotImplementedException();
		}
	}
}
