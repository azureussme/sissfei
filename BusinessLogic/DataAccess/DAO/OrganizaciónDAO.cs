﻿using BusinessLogic.DataAccess.Interface;
using BusinessLogic.Model;
using Database;
using System;

namespace BusinessLogic.DataAccess.DAO
{
	public class OrganizaciónDAO : IOrganizaciónDAO
	{
		public int AgregarOrganización(Organización organización)
		{
			var command = new Command(
				"INSERT INTO SISSFEI.Organización " +
					"(Dirección, Nombre, Teléfono) " +
				"VALUES " +
					"(@Dirección, @Nombre, @Teléfono);"
			);

			command.AddParameter("@Dirección", organización.Dirección);
			command.AddParameter("@Nombre", organización.Nombre);
			command.AddParameter("@Teléfono", organización.Teléfono);

			var result = command.Execute();

			if (result == 0)
			{
				throw new Exception("No se pudo agregar la organización");
			}

			var IdOrganización = command.GetLastInsertId();
			organización.IdOrganización = IdOrganización;

			return IdOrganización;
		}

		public void EditarOrganización(Organización organización)
		{
			throw new NotImplementedException();
		}

		public void EliminarOrganización(int IdOrganización)
		{
			throw new NotImplementedException();
		}
	}
}
