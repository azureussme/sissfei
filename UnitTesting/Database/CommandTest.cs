using Database;
using Xunit;

namespace UnitTest.Database
{
    public class CommandTest
    {
        [Fact]
        public void TestCanCreateCommand()
        {
            var command = new Command("SELECT 1;");
        }

        [Fact]
        public void TestCanCreateCommandWithoutQuery()
        {
            var command = new Command();
            command.CreateQuery("SELECT 1;");
        }

        [Fact]
        public void TestCanAddParameter()
        {
            var command = new Command("SELECT @id;");
            command.AddParameter("@id", 1);
        }

        [Fact]
        public void TestCanExecuteQuery()
        {
            var command = new Command("SELECT 1");
            command.Execute();
        }
    }
}