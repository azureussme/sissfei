using System;
using Database;

namespace UnitTest.BusinessLogic.DataAccess
{
    public class DaoBaseTest : IDisposable
    {
        protected DaoBaseTest()
        {
            DatabaseS.Instance.BeginTransaction();
        }

        public void Dispose()
        {
            DatabaseS.Instance.RollbackTransaction();
        }
    }
}