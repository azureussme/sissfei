using System;
using BusinessLogic.DataAccess.DAO;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTest.BusinessLogic.DataAccess
{
    public class PersonaDAOTest : DaoBaseTest
    {

        [Fact]
        public void TestCanCreatePersona()
        {
            var técnicoAcadémico = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = "16011696",
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            var personaDao = new PersonaDAO();

            var idPersona = personaDao.AgregarPersona(técnicoAcadémico);

            Assert.True(idPersona > 0);
        }

        [Fact]
        public void TestCanEditPersona()
        {
            var técnicoAcadémico = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = "16011696",
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            var personaDao = new PersonaDAO();

            personaDao.AgregarPersona(técnicoAcadémico);

            técnicoAcadémico.Email = "azureussme@hotmail.com";

            personaDao.EditarPersona(técnicoAcadémico);
        }
        
        [Fact]
        public void TestCantEditPersonaWithoutId()
        {
            var técnicoAcadémico = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = "16011696",
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            var personaDao = new PersonaDAO();
            var exception = Assert.Throws<Exception>(() => personaDao.EditarPersona(técnicoAcadémico));
            Assert.Equal("Error al editar persona, no cuenta con ID", exception.Message);
        }
        
    }
}