using BusinessLogic.DataAccess.DAO;
using BusinessLogic.Model;
using Xunit;

namespace UnitTest.BusinessLogic.DataAccess
{
    public class ProyectoDAOTest : DaoBaseTest
    {

        [Fact]
        public void TestCanCreateProyectoWithoutEncargado()
        {
            var organización = new Organización
            {
                Nombre = "TELMEX",
                Dirección = "Enriquez #101, Col. Centro, Xalapa, Ver.",
                Teléfono = "228 214 61 35"
            };

            var organizaciónDao = new OrganizaciónDAO();
            var idOrganización = organizaciónDao.AgregarOrganización(organización);

            var proyecto = new Proyecto
            {
                Actividad = "Hacer magia con sus manitas",
                Description = "Diseñar software para hacer magia en mi empresa",
                Duración = "6 largos meses",
                FunciónActividad = "Desarrollador JR.",
                Metodología = "Pues la que más le guste",
                Nombre = "Creación de software para empresa",
                NúmeroParticipantes = 5,
                Requisitos = "Ser estudiante activo y con muchas ganas",
                HorarioProbable = "Por las mañanas"
            };

            var proyectoDao = new ProyectoDAO();
            var idProyecto = proyectoDao.AgregarProyecto(proyecto, idOrganización);

            Assert.True(idProyecto > 0);
        }
        
    }
}