using System;
using System.Collections.Generic;
using BusinessLogic.DataAccess.DAO;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;
namespace UnitTest.BusinessLogic.DataAccess
{
    public class HorarioDAOTest : DaoBaseTest
    {
        
        [Fact]
        public void TestCanDeleteHorarioFromTécnicoAcadémico()
        {
            const string númeroPersonal = "horario0123";

            var horarios = new List<Horario>();

            var horarioLunes = new Horario
            {
                DíaSemana = DíaSemana.Domingo,
                HoraInicio = "06:00",
                HoraFin = "12:00",
                Lugar = "En mi casita"
            };

            horarios.Add(horarioLunes);

            var técnicoAcadémico = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = númeroPersonal,
                Horarios = horarios,
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            var técnicoAcadémicoDao = new TécnicoAcadémicoDAO();
            técnicoAcadémicoDao.AgregarTécnicoAcadémico(técnicoAcadémico);

            var horarioDao = new HorarioDAO();
            horarioDao.EliminarHorarioDeTécnicoAcadémico(técnicoAcadémico.Horarios[0].IdHorario);
            técnicoAcadémico.Horarios.RemoveAt(0);
        }

        [Fact]
        public void TestCanDeleteHorarioFromProyecto()
        {
            var organización = new Organización
            {
                Nombre = "TELMEX",
                Dirección = "Enriquez #101, Col. Centro, Xalapa, Ver.",
                Teléfono = "228 214 61 35"
            };

            var organizaciónDao = new OrganizaciónDAO();
            var idOrganización = organizaciónDao.AgregarOrganización(organización);

            var proyecto = new Proyecto
            {
                Actividad = "Hacer magia con sus manitas",
                Description = "Diseñar software para hacer magia en mi empresa",
                Duración = "6 largos meses",
                FunciónActividad = "Desarrollador JR.",
                Metodología = "Pues la que más le guste",
                Nombre = "Creación de software para empresa",
                NúmeroParticipantes = 5,
                Requisitos = "Ser estudiante activo y con muchas ganas",
                HorarioProbable = "Por las mañanas",
                Horarios = new List<Horario>()
            };


            var horario = new Horario
            {
                DíaSemana = DíaSemana.Domingo,
                HoraInicio = "06:00",
                HoraFin = "12:00",
                Lugar = "En mi casita"
            };

            proyecto.Horarios.Add(horario);

            var proyectoDao = new ProyectoDAO();
            proyectoDao.AgregarProyecto(proyecto, idOrganización);

            var horarioDao = new HorarioDAO();
            horarioDao.EliminarHorarioDeProyecto(proyecto.Horarios[0].IdHorario);
            proyecto.Horarios.RemoveAt(0);
        }

        [Fact]
        public void TestCantDeleteHorarioWithInvalidId()
        {
            const int idHorario = -1;
            var horarioDao = new HorarioDAO();

            var exception = Assert.Throws<Exception>(() => horarioDao.EliminarHorarioDeProyecto(idHorario));
            Assert.Equal("No se pudo eliminar horario de la tabla vinculante Proyecto con el ID: -1", exception.Message);
        }
    }
}