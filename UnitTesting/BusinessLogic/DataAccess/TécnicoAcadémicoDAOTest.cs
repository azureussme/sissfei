using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using BusinessLogic.DataAccess.DAO;
using BusinessLogic.Model;
using BusinessLogic.Model.Enum;
using Xunit;

namespace UnitTest.BusinessLogic.DataAccess
{
    public class TécnicoAcadémicoDAOTest : DaoBaseTest
    {

        [Fact]
        public void TestCanCreateTécnicoAcadémico()
        {
            const string númeroPersonal = "TATest02";

            var técnicoAcadémico = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = númeroPersonal,
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            var técnicoAcadémicoDao = new TécnicoAcadémicoDAO();
            var idTécnicoAcadémico = técnicoAcadémicoDao.AgregarTécnicoAcadémico(técnicoAcadémico);

            Assert.True(idTécnicoAcadémico > 0);
        }

        [Fact]
        public void TestCantCreateTwoTécnicosAcadémicosWithSameNúmeroPersonal()
        {
            const string númeroPersonal = "TATestUnique";

            var técnicoAcadémicoDao = new TécnicoAcadémicoDAO();

            var técnicoAcadémico1 = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = númeroPersonal,
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            técnicoAcadémicoDao.AgregarTécnicoAcadémico(técnicoAcadémico1);

            var técnicoAcadémico2 = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = númeroPersonal,
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            // TODO Custom Exception
            var exception = Assert.Throws<SqlException>(() => técnicoAcadémicoDao.AgregarTécnicoAcadémico(técnicoAcadémico2));
            Assert.Equal(typeof(SqlException), exception.GetType());
        }
        
        [Fact]
        public void TestCanCreateTécnicoAcadémicoWithHorarios()
        {
            const string númeroPersonal = "TATEST033";

            var horarios = new List<Horario>();

            var horarioLunes = new Horario
            {
                DíaSemana = DíaSemana.Lunes,
                HoraInicio = "09:00",
                HoraFin = "13:00",
                Lugar = "Cubículo 32"
            };

            var horarioMartes = new Horario
            {
                DíaSemana = DíaSemana.Martes,
                HoraInicio = "09:00",
                HoraFin = "13:00",
                Lugar = "Cubículo 32"
            };

            horarios.Add(horarioLunes);
            horarios.Add(horarioMartes);

            var técnicoAcadémico = new TécnicoAcadémico
            {
                Nombre = "Samuel Elías",
                ApellidoPaterno = "Gaona",
                ApellidoMaterno = "Hernández",
                Email = "azureussme@gmail.com",
                FechaNacimiento = new DateTime(1993, 2, 16),
                Género = Género.Masculino,
                NúmeroPersonal = númeroPersonal,
                Horarios = horarios,
                ProgramaEducativo = ProgramaEducativo.IngenieríaDeSoftware
            };

            var técnicoAcadémicoDao = new TécnicoAcadémicoDAO();
            var idTécnicoAcadémico = técnicoAcadémicoDao.AgregarTécnicoAcadémico(técnicoAcadémico);

            Assert.True(idTécnicoAcadémico > 0);
        }
        
    }
}