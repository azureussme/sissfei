using BusinessLogic.DataAccess.DAO;
using BusinessLogic.Model;
using Xunit;

namespace UnitTest.BusinessLogic.DataAccess
{
    public class OrganizaciónDAOTest : DaoBaseTest
    {

        [Fact]
        public void TestCanCreateOrganizaciónWithoutEncargado()
        {
            var organización = new Organización
            {
                Nombre = "TELMEX",
                Dirección = "Enriquez #101, Col. Centro, Xalapa, Ver.",
                Teléfono = "228 214 61 35"
            };

            var organizaciónDao = new OrganizaciónDAO();
            var idOrganización = organizaciónDao.AgregarOrganización(organización);

            Assert.True(idOrganización > 0);
        }
        
    }
}