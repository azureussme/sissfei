﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Database
{
	/// <summary>
	/// Clase envoltoria (wrapper) que maneja los commandos SQL.
	/// </summary>
	public class Command
	{
		private SqlCommand sqlCommand;

		public Command()
		{

		}

		/// <summary>
		/// Crea un comando SQL.
		/// </summary>
		/// <param name="query">La consulta SQL</param>
		public Command(string query)
		{
			CreateQuery(query);
		}

		public void CreateQuery(string query)
		{
			sqlCommand = DatabaseS.Instance.CreateCommand(query);
		}

		/// <summary>
		/// Agrega un parámetro al comando SQL.
		/// </summary>
		/// <param name="parameterName">Nombre del parámetro</param>
		/// <param name="value">Valor del parámetro</param>
		public void AddParameter(string parameterName, object value)
		{
			sqlCommand.Parameters.AddWithValue(parameterName, value);
		}

		/// <summary>
		/// Ejecuta una instrucción INSERT, UPDATE o DELETE en la base de datos
		/// </summary>
		/// <returns>El número de filas afectadas</returns>
		public int Execute()
		{
			return sqlCommand.ExecuteNonQuery();
		}

		public int GetLastInsertId()
		{
			CreateQuery("SELECT @@IDENTITY");
			return Convert.ToInt32(sqlCommand.ExecuteScalar());
		}

		/// <summary>
		/// Realiza un SELECT en la base de datos
		/// </summary>
		/// <returns>Los resultados de la consulta.</returns>
		public DataTable Query()
		{
			var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
			var dataTable = new DataTable();
			sqlDataAdapter.Fill(dataTable);
			return dataTable;
		}
	}
}
