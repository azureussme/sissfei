﻿using System.Configuration;
using System.Data.SqlClient;

namespace Database
{
	/// <summary>
	/// La clase Database es un Singleton que se encarga de realizar la conexión SQL al servidor SQL Server
	/// del SISSFEI
	/// </summary>
	public sealed class DatabaseS
	{
		/// <summary>
		/// Conexión a la base de datos SQL Server
		/// </summary>
		private static SqlConnection sqlConnection;
		private static SqlTransaction sqlTransaction;

		static DatabaseS()
		{

		}

		private DatabaseS()
		{

		}

		/// <summary>
		/// Única instancia de la clase DB
		/// </summary>
		public static DatabaseS Instance { get; } = new DatabaseS();

		/// <summary>
		/// Devuelve la única conexión SQL de la base de datos SQL Server. La inicializa si es necesario.
		/// </summary>
		/// <returns>Conexión SQL de la base de datos SQL Server.</returns>
		private SqlConnection GetConnection()
		{
			if (sqlConnection == null)
			{
				var connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
				sqlConnection = new SqlConnection(connectionString);
				sqlConnection.Open();
			}

			return sqlConnection;
		}

		public SqlCommand CreateCommand(string query)
		{
			if (sqlTransaction != null)
			{
				return new SqlCommand(query, GetConnection(), sqlTransaction);
			}
			else
			{
				return new SqlCommand(query, GetConnection());
			}
		}

		public void BeginTransaction()
		{
			sqlTransaction = GetConnection().BeginTransaction();
		}

		public void RollbackTransaction()
		{
			if (sqlTransaction != null)
			{
				sqlTransaction.Rollback();
				sqlTransaction = null;
			}
		}

		public void CommitTransaction()
		{
			if (sqlTransaction != null)
			{
				sqlTransaction.Commit();
				sqlTransaction = null;
			}
		}
	}
}
